package com.hackathon.news.domain;

import com.hackathon.config.DeletedEvent;
import com.hackathon.config.SequenceGeneratorService;
import com.hackathon.news.dto.NewsDto;
import com.hackathon.news.dto.NewsNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.mongodb.core.query.TextCriteria;

import java.util.List;
import java.util.stream.Collectors;

public class NewsFacade {

  @Autowired
  private ApplicationEventPublisher applicationEventPublisher;

  @Autowired
  SequenceGeneratorService sequenceGeneratorService;

  private NewsCreator newsCreator;
  private NewsRepository newsRepository;

  public NewsFacade(NewsCreator newsCreator, NewsRepository newsRepository) {
    this.newsCreator = newsCreator;
    this.newsRepository = newsRepository;
  }

    public NewsDto addNewNews(NewsDto newsDto) {
        long id = sequenceGeneratorService.generateSequence(News.SEQUENCE_NAME);
        News news = newsRepository.save(newsCreator.from(newsDto, id));
        applicationEventPublisher.publishEvent(news.dto());
        return news.dto();
    }

  public List<NewsDto> getNews() {
    return newsRepository.findAll()
        .stream()
        .map(News::dto)
        .collect(Collectors.toList());
  }

  public NewsDto get(long id) {
    return newsRepository.findById(id)
            .orElseThrow(() -> new NewsNotFoundException(id)).dto();
  }

  public void delete(long id) {
    newsRepository.deleteById(id);
    applicationEventPublisher.publishEvent(new DeletedEvent(id));
  }

  public List<NewsDto> find(String searchParam) {
    TextCriteria criteria = TextCriteria.forDefaultLanguage()
            .matchingAny(searchParam);
    return newsRepository.findAllBy(criteria)
            .stream().map(News::dto)
            .collect(Collectors.toList());
  }

}

