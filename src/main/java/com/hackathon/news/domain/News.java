package com.hackathon.news.domain;

import com.hackathon.news.dto.NewsDto;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Builder
@Document(collection = "news")
@FieldDefaults(level = AccessLevel.PRIVATE)
class News {
  @Id
  Long id;
  @NotNull
  @TextIndexed
  String title;
  @NotNull
  String text;
  @NotNull
  String author;
  @NotNull
  Date data;
  String fileName;

  @Transient
  public static final String SEQUENCE_NAME = "news_sequence";

  NewsDto dto(){
    return NewsDto.builder()
        .id(id)
        .title(title)
        .text(text)
        .data(data)
        .author(author)
        .fileName(fileName)
        .build();
  }
}
