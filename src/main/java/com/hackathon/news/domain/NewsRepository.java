package com.hackathon.news.domain;

import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface NewsRepository extends MongoRepository<News, Long> {

    List<News> findAllBy(TextCriteria criteria);
}
