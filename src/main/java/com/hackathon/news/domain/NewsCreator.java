package com.hackathon.news.domain;

import com.hackathon.news.dto.NewsDto;

class NewsCreator {

  News from(NewsDto newsDto){
    return News.builder()
        .id(newsDto.getId())
        .author(newsDto.getAuthor())
        .data(newsDto.getData())
        .text(newsDto.getText())
        .title(newsDto.getTitle())
        .fileName(newsDto.getFileName())
        .build();
  }

  News from(NewsDto newsDto, long id){
    return News.builder()
        .id(id)
        .author(newsDto.getAuthor())
        .data(newsDto.getData())
        .text(newsDto.getText())
        .title(newsDto.getTitle())
        .fileName(newsDto.getFileName())
        .build();
  }
}
