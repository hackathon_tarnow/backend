package com.hackathon.news.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class NewsConfigurator {

  @Bean
  NewsFacade newsFacade(NewsRepository newsRepository) {
    NewsCreator newsCreator = new NewsCreator();
    return new NewsFacade(newsCreator, newsRepository);
  }
}
