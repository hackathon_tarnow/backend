package com.hackathon.news;

import com.hackathon.news.domain.NewsFacade;
import com.hackathon.news.dto.NewsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/news")
public class NewsController {

  @Autowired
  private NewsFacade newsFacade;

  @GetMapping
  public ResponseEntity<List<NewsDto>> get(@RequestParam(required=false) String search) {
        if (search != null) {
          return ResponseEntity.ok(newsFacade.find(search));
        }
    return ResponseEntity.ok(newsFacade.getNews());
  }

  @GetMapping("/{id}")
  public ResponseEntity<NewsDto> get(@PathVariable Long id) {
    return ResponseEntity.ok(newsFacade.get(id));
  }

  @PostMapping
  public ResponseEntity<Void> add(@RequestBody NewsDto newsDto) {
    newsFacade.addNewNews(newsDto);
    return ResponseEntity.noContent().build();
  }

  @PostMapping("/batch")
  public ResponseEntity<Void> add(@RequestBody List<NewsDto> newsDto) {
    newsDto.forEach(newsFacade::addNewNews);
    return ResponseEntity.noContent().build();
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> delete(@PathVariable Long id) {
    newsFacade.delete(id);
    return ResponseEntity.noContent().build();
  }
}