package com.hackathon.news.dto;

public class NewsNotFoundException extends RuntimeException {
    public NewsNotFoundException(Long id) {
        super("No news with id" + id + " found", null, false, false);
    }
}