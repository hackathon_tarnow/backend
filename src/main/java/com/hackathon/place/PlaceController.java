package com.hackathon.place;

import com.hackathon.place.domain.PlaceFacade;
import com.hackathon.place.dto.PlaceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/places")
public class PlaceController {

    @Autowired
    private PlaceFacade placeFacade;

    @GetMapping
    public ResponseEntity<List<PlaceDto>> get(@RequestParam(required=false) String search) {
        if (search != null) {
            return ResponseEntity.ok(placeFacade.find(search));
        }
        return ResponseEntity.ok(placeFacade.get());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PlaceDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(placeFacade.get(id));
    }

    @PostMapping
    public ResponseEntity<Void> add(@RequestBody PlaceDto placeDto) {
        placeFacade.add(placeDto);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/batch")
    public ResponseEntity<Void> add(@RequestBody List<PlaceDto> placeDto) {
        placeDto.forEach(placeFacade::add);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        placeFacade.delete(id);
        return ResponseEntity.noContent().build();
    }
}
