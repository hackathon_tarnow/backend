package com.hackathon.place.domain;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.TextCriteria;

import java.util.*;

class InMemoryRepository implements PlaceRepository {

    private Map<Long, Place> places = new HashMap<>();

    @Override
    public Place findById(long id) {
        return places.get(id);
    }

    @Override
    public Place save(Place place) {
        places.put(place.dto().getId(), place);
        return place;
    }

    @Override
    public <S extends Place> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Place> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public List<Place> findAll() {
        return new ArrayList<>(places.values());
    }

    @Override
    public List<Place> findAllBy(TextCriteria criteria) {
        return null;
    }

    @Override
    public Iterable<Place> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Place place) {

    }

    @Override
    public void deleteAll(Iterable<? extends Place> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public List<Place> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Place> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Place> S insert(S s) {
        return null;
    }

    @Override
    public <S extends Place> List<S> insert(Iterable<S> iterable) {
        return null;
    }

    @Override
    public <S extends Place> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Place> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Place> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Place> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Place> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Place> boolean exists(Example<S> example) {
        return false;
    }
}
