package com.hackathon.place.domain;

import com.hackathon.place.dto.PlaceDto;
import com.hackathon.place.dto.PlaceTypeDto;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Builder
@Document(collection = "place")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Place {

    @Id
    long id;
    @NotNull
    @TextIndexed
    String name;
    @NotNull
    String content;
    @NotNull
    Double latitude;
    @NotNull
    Double longitude;
    String fileName;
    boolean isCityCardAllowed;
    PlaceType placeType;

    @Transient
    public static final String SEQUENCE_NAME = "place_sequence";

    PlaceDto dto() {
        return PlaceDto.builder()
                .id(id)
                .name(name)
                .content(content)
                .longitude(longitude)
                .latitude(latitude)
                .fileName(fileName)
                .isCityCardAllowed(isCityCardAllowed)
                .placeTypeDto(PlaceTypeDto.valueOf(placeType.name()))
                .build();
    }

}
