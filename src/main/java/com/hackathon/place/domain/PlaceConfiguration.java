package com.hackathon.place.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class PlaceConfiguration {

    PlaceFacade placeFacade() {
        return placeFacade(new InMemoryRepository());
    }

    @Bean
    PlaceFacade placeFacade(PlaceRepository placeRepository) {
        PlaceCreator placeCreator = new PlaceCreator();
        return new PlaceFacade(placeRepository, placeCreator);
    }
}
