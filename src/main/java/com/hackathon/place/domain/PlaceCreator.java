package com.hackathon.place.domain;

import com.hackathon.place.dto.PlaceDto;

import static java.util.Objects.requireNonNull;

public class PlaceCreator {

    Place from(PlaceDto placeDto) {
        requireNonNull(placeDto);
        return Place.builder()
                .id(placeDto.getId())
                .name(placeDto.getName())
                .content(placeDto.getContent())
                .latitude(placeDto.getLatitude())
                .longitude(placeDto.getLongitude())
                .build();
    }
    Place from(PlaceDto placeDto, long id) {
        requireNonNull(placeDto);
        return Place.builder()
                .id(id)
                .name(placeDto.getName())
                .content(placeDto.getContent())
                .latitude(placeDto.getLatitude())
                .longitude(placeDto.getLongitude())
                .fileName(placeDto.getFileName())
                .isCityCardAllowed(placeDto.getIsCityCardAllowed())
                .placeType(PlaceType.valueOf(placeDto.getPlaceTypeDto().name()))
                .build();
    }
}
