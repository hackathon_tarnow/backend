package com.hackathon.place.domain;

import com.hackathon.config.DeletedEvent;
import com.hackathon.config.SequenceGeneratorService;
import com.hackathon.place.dto.PlaceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.mongodb.core.query.TextCriteria;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

public class PlaceFacade {

    private PlaceRepository placeRepository;

    private PlaceCreator placeCreator;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    public PlaceFacade(PlaceRepository placeRepository, PlaceCreator placeCreator) {
        this.placeRepository = placeRepository;
        this.placeCreator = placeCreator;
    }

    public void add(PlaceDto placeDto) {
        requireNonNull(placeDto);
        long id = sequenceGeneratorService.generateSequence(Place.SEQUENCE_NAME);
        Place place = placeRepository.save(placeCreator.from(placeDto, id));
        applicationEventPublisher.publishEvent(place.dto());
    }

    public PlaceDto get(long id) {
        return placeRepository.findOneOrThrow(id).dto();
    }

    public List<PlaceDto> get() {
        return placeRepository.findAll()
                .stream()
                .map(Place::dto)
                .collect(Collectors.toList());
    }

    public void delete(long id) {
        placeRepository.deleteById(id);
        applicationEventPublisher.publishEvent(new DeletedEvent(id));
    }

    public List<PlaceDto> find(String searchParam) {
        TextCriteria criteria = TextCriteria.forDefaultLanguage()
                .matchingAny(searchParam);
        return placeRepository.findAllBy(criteria)
                .stream().map(Place::dto)
                .collect(Collectors.toList());
    }

}
