package com.hackathon.place.domain;

import com.hackathon.place.dto.PlaceNotFoundException;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PlaceRepository extends MongoRepository<Place, Long> {

    Place findById(long id);

    Place save(Place place);

    List<Place> findAll();

    List<Place> findAllBy(TextCriteria criteria);

    default Place findOneOrThrow(Long id) {
        return findById(id)
                .orElseThrow(() -> new PlaceNotFoundException(id));
    }
}
