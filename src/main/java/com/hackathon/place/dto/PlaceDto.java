package com.hackathon.place.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.context.ApplicationEvent;

@Builder
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PlaceDto{

    Long id;
    String name;
    String content;
    Double latitude;
    Double longitude;
    String fileName;
    Boolean isCityCardAllowed;
    PlaceTypeDto placeTypeDto;
}
