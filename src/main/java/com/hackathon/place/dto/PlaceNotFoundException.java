package com.hackathon.place.dto;

public class PlaceNotFoundException extends RuntimeException {
    public PlaceNotFoundException(Long id) {
        super("No place with id" + id + " found", null, false, false);
    }
}
