package com.hackathon.event;

import com.hackathon.event.domain.EventFacade;
import com.hackathon.event.dto.EventDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/events")
class EventController {
  private EventFacade eventFacade;

  public EventController(EventFacade eventFacade) {
    this.eventFacade = eventFacade;
  }

  @GetMapping
  ResponseEntity<List<EventDto>> getEvents(@RequestParam(required = false) String search){
    if(search != null){
      return ResponseEntity.ok(eventFacade.find(search));
    }
    return ResponseEntity.ok(eventFacade.get());
  }

  @GetMapping("/{id}")
  ResponseEntity<EventDto> getEvents(@PathVariable Long id){
    return ResponseEntity.ok(eventFacade.get(id));
  }

  @PostMapping("/batch")
  public ResponseEntity<Void> addEvents(@RequestBody List<EventDto> newsDto) {
    newsDto.forEach(eventFacade::addNewEvent);
    return ResponseEntity.noContent().build();
  }

  @PostMapping
  public ResponseEntity<EventDto> createEvent(@RequestBody EventDto eventDto) {
    eventFacade.addNewEvent(eventDto);
    return ResponseEntity.ok(eventDto);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> delete(@PathVariable Long id){
    eventFacade.delete(id);
    return ResponseEntity.noContent().build();
  }


}
