package com.hackathon.event.dto;

public class EventNotFoundException extends RuntimeException {
    public EventNotFoundException(Long id) {
        super("No event with id" + id + " found", null, false, false);
    }
}