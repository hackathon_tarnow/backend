package com.hackathon.event.domain;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.TextCriteria;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

class InMemoryEventRepository implements EventRepository {

  private ConcurrentHashMap<Long, Event> map = new ConcurrentHashMap<>();
  private long id = 0;

  @Override
  public <S extends Event> S save(S s) {
    long idToSave = s.dto().getId() != null ? s.dto().getId() : id++;
    map.put(idToSave, s);
    return s;
  }

  @Override
  public <S extends Event> List<S> saveAll(Iterable<S> iterable) {
    return null;
  }

  @Override
  public Optional<Event> findById(Long aLong) {
    return Optional.empty();
  }

  @Override
  public boolean existsById(Long aLong) {
    return false;
  }

  @Override
  public List<Event> findAll() {
    return new ArrayList<>(map.values());
  }

  @Override
  public Iterable<Event> findAllById(Iterable<Long> iterable) {
    return null;
  }

  @Override
  public long count() {
    return 0;
  }

  @Override
  public void deleteById(Long aLong) {

  }

  @Override
  public void delete(Event event) {

  }

  @Override
  public void deleteAll(Iterable<? extends Event> iterable) {

  }

  @Override
  public void deleteAll() {

  }

  @Override
  public List<Event> findAll(Sort sort) {
    return null;
  }

  @Override
  public Page<Event> findAll(Pageable pageable) {
    return null;
  }

  @Override
  public <S extends Event> S insert(S s) {
    return null;
  }

  @Override
  public <S extends Event> List<S> insert(Iterable<S> iterable) {
    return null;
  }

  @Override
  public <S extends Event> Optional<S> findOne(Example<S> example) {
    return Optional.empty();
  }

  @Override
  public <S extends Event> List<S> findAll(Example<S> example) {
    return null;
  }

  @Override
  public <S extends Event> List<S> findAll(Example<S> example, Sort sort) {
    return null;
  }

  @Override
  public <S extends Event> Page<S> findAll(Example<S> example, Pageable pageable) {
    return null;
  }

  @Override
  public <S extends Event> long count(Example<S> example) {
    return 0;
  }

  @Override
  public <S extends Event> boolean exists(Example<S> example) {
    return false;
  }

  @Override
  public List<Event> findAllBy(TextCriteria criteria) {
    return null;
  }
}
