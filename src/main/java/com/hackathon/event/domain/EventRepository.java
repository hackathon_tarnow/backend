package com.hackathon.event.domain;

import com.hackathon.event.dto.EventNotFoundException;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface EventRepository extends MongoRepository<Event, Long> {

    List<Event> findAllBy(TextCriteria criteria);
}
