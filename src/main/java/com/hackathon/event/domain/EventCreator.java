package com.hackathon.event.domain;

import com.hackathon.event.dto.EventDto;
import lombok.AllArgsConstructor;


import static java.util.Objects.requireNonNull;

@AllArgsConstructor
class EventCreator {

  private EventRepository eventRepository;

  Event from(EventDto eventDto){
    requireNonNull(eventDto);
    return Event.builder()
        .id(eventDto.getId())
        .name(eventDto.getName())
        .eventDate(eventDto.getEventDate())
        .eventDescription(eventDto.getEventDescription())
        .fileName(eventDto.getFileName())
        .longitude(eventDto.getLongitude())
        .latitude(eventDto.getLatitude())
        .build();
  }

  Event from(EventDto eventDto, long id){
    requireNonNull(eventDto);
    return Event.builder()
            .id(id)
            .name(eventDto.getName())
            .eventDate(eventDto.getEventDate())
            .eventDescription(eventDto.getEventDescription())
            .fileName(eventDto.getFileName())
            .longitude(eventDto.getLongitude())
            .latitude(eventDto.getLatitude())
            .build();
  }


}
