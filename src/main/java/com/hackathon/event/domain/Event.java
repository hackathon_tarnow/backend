package com.hackathon.event.domain;

import com.hackathon.event.dto.EventDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Document(collection = "event")
class Event {
  @Id
  private Long id;
  private String name;
  private Date eventDate;
  @TextIndexed
  private String eventDescription;
  private String fileName;
  private Double latitude;
  private Double longitude;

  @Transient
  public static final String SEQUENCE_NAME = "event_sequence";

  EventDto dto() {
    return EventDto.builder()
        .id(id)
        .name(name)
        .eventDate(eventDate)
        .eventDescription(eventDescription)
        .fileName(fileName)
        .latitude(latitude)
        .longitude(longitude)
        .build();
  }
}
