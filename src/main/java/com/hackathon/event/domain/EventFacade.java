/*
 *WARNING. HCM Deck sp. z o.o. is the only holder of intellectual property rights of this source code.
 *HCM Deck sp. z o.o. does not issue under any circumstances its source codes
 *to anyone other than HCM Deck sp. z o.o. and does not give a right to view,
 *modify, edit or any other form of interference in the source code.
 *If you got an access to this source code, immediately inform
 *HCM Deck sp. z o.o. Any action contradictory to this reservation will result in the enforcement of all rights of HCM Deck sp. z o.o.
 *Violation of copyright will result in liability for damages caused to HCM Deck sp. z o.o. and may lead to criminal liability under
 *the Act of 4 February 1994 on Copyright and Related Rights
 *(Journal of Laws 1994 No. 24, item 83, as amended) (Articles 115 - 118) including up to 5 years of imprisonment.
 */

package com.hackathon.event.domain;

import com.hackathon.config.SequenceGeneratorService;
import com.hackathon.config.DeletedEvent;
import com.hackathon.event.dto.EventDto;
import com.hackathon.event.dto.EventNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.mongodb.core.query.TextCriteria;

import java.util.List;
import java.util.stream.Collectors;

public class EventFacade {

  @Autowired
  private ApplicationEventPublisher applicationEventPublisher;

  @Autowired
  SequenceGeneratorService sequenceGeneratorService;

  private EventRepository eventRepository;
  private EventCreator eventCreator;

  public EventFacade(EventRepository eventRepository, EventCreator eventCreator) {
    this.eventRepository = eventRepository;
    this.eventCreator = eventCreator;
  }

  public EventDto addNewEvent(EventDto eventDto){
    long id = sequenceGeneratorService.generateSequence(Event.SEQUENCE_NAME);
    Event newEvent = eventRepository.save(eventCreator.from(eventDto, id));
    eventDto = newEvent.dto();
    applicationEventPublisher.publishEvent(eventDto);
    return eventDto;
  }

  public List<EventDto> find(String searchParam) {
    TextCriteria criteria = TextCriteria.forDefaultLanguage()
            .matchingAny(searchParam);
    return eventRepository.findAllBy(criteria)
            .stream().map(Event::dto)
            .collect(Collectors.toList());
  }

  public List<EventDto> get() {
    return eventRepository.findAll()
        .stream()
        .map(Event::dto)
        .collect(Collectors.toList());
  }

  public EventDto get(Long id){
    return eventRepository.findById(id)
            .orElseThrow(() -> new EventNotFoundException(id)).dto();
  }

  public void delete(Long id){
    eventRepository.deleteById(id);
    applicationEventPublisher.publishEvent(new DeletedEvent(id));
  }
}
