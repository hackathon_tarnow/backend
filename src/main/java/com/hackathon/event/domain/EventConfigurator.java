package com.hackathon.event.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class EventConfigurator {

  EventFacade eventFacade(){
    return eventFacade(new InMemoryEventRepository());
  }

  @Bean
  EventFacade eventFacade(EventRepository eventRepository){
    EventCreator categoryCreator = new EventCreator(eventRepository);
    return new EventFacade(eventRepository, categoryCreator);
  }
}
