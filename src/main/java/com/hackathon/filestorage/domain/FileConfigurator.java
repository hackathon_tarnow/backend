package com.hackathon.filestorage.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class FileConfigurator {

  @Bean
  FileFacade fileFacade(){
    FileSystemStorage fileSystemStorage = new FileSystemStorage();
    return new FileFacade(fileSystemStorage);
  }
}
