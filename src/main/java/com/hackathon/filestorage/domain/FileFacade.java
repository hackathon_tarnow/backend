package com.hackathon.filestorage.domain;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;


public class FileFacade {
  private FileSystemStorage fileSystemStorage;

  public FileFacade(FileSystemStorage fileSystemStorage) {
    this.fileSystemStorage = fileSystemStorage;
    this.fileSystemStorage.init();
  }

  public void store(MultipartFile file){
    fileSystemStorage.store(file);
  }

  public Resource loadAsResource(String filename){
    return fileSystemStorage.loadAsResource(filename);
  }
}
