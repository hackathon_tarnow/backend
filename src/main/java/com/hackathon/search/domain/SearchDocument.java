package com.hackathon.search.domain;


import com.hackathon.search.dto.DocumentType;
import com.hackathon.search.dto.SearchDocumentDto;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Builder
@Document(collection = "searchdocument")
@FieldDefaults(level = AccessLevel.PRIVATE)
class SearchDocument {

    @Id
    Long id;

    @TextIndexed
    String value;

    Long objectId;

    DocumentType type;

    @Transient
    public static final String SEQUENCE_NAME = "searchdocument_sequence";

    SearchDocumentDto dto() {
        return SearchDocumentDto.builder().id(id)
                .type(type).value(value)
                .objectId(objectId)
                .build();
    }
}
