package com.hackathon.search.domain;

import com.hackathon.config.SequenceGeneratorService;
import com.hackathon.config.DeletedEvent;
import com.hackathon.event.dto.EventDto;
import com.hackathon.news.dto.NewsDto;
import com.hackathon.place.dto.PlaceDto;
import com.hackathon.report.dto.ReportDto;
import com.hackathon.search.dto.DocumentType;
import com.hackathon.search.dto.SearchDocumentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.data.mongodb.core.query.TextCriteria;

import java.util.List;
import java.util.stream.Collectors;

public class SearchFacade {

    private SearchRepository searchRepository;

    private SearchDocumentCreator searchDocumentCreator;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    public SearchFacade(SearchRepository searchRepository, SearchDocumentCreator searchDocumentCreator) {
        this.searchRepository = searchRepository;
        this.searchDocumentCreator = searchDocumentCreator;
    }

    @EventListener
    public void handlePlaceDto(PlaceDto placeDto) {
        SearchDocumentDto searchDocumentDto = SearchDocumentDto.builder()
                .objectId(placeDto.getId())
                .type(DocumentType.PLACE)
                .value(placeDto.getName())
                .build();
        long id = sequenceGeneratorService.generateSequence(SearchDocument.SEQUENCE_NAME);
        searchRepository.save(searchDocumentCreator.from(searchDocumentDto, id));
    }

    @EventListener
    public void handleNewsDto(NewsDto newsDto) {
        SearchDocumentDto searchDocumentDto = SearchDocumentDto.builder()
                .objectId(newsDto.getId())
                .type(DocumentType.NEWS)
                .value(newsDto.getTitle())
                .build();
        long id = sequenceGeneratorService.generateSequence(SearchDocument.SEQUENCE_NAME);
        searchRepository.save(searchDocumentCreator.from(searchDocumentDto, id));
    }

    @EventListener
    public void handleEventDto(EventDto eventDto) {
        SearchDocumentDto searchDocumentDto = SearchDocumentDto.builder()
                .objectId(eventDto.getId())
                .type(DocumentType.EVENT)
                .value(eventDto.getName())
                .build();
        long id = sequenceGeneratorService.generateSequence(SearchDocument.SEQUENCE_NAME);
        searchRepository.save(searchDocumentCreator.from(searchDocumentDto, id));
    }

    @EventListener
    public void handleReportDto(ReportDto reportDto) {
        SearchDocumentDto searchDocumentDto = SearchDocumentDto.builder()
                .objectId(reportDto.getId())
                .type(DocumentType.REPORT)
                .value(reportDto.getTitle())
                .build();
        long id = sequenceGeneratorService.generateSequence(SearchDocument.SEQUENCE_NAME);
        searchRepository.save(searchDocumentCreator.from(searchDocumentDto, id));
    }

    @EventListener
    public void handleDeletedEvent(DeletedEvent deletedEvent) {
        searchRepository.deleteByObjectId(deletedEvent.getId());
    }

    public List<SearchDocumentDto> find(String searchParam) {
        TextCriteria criteria = TextCriteria.forDefaultLanguage()
                .matchingAny(searchParam);
        return searchRepository.findAllBy(criteria)
                .stream().map(SearchDocument::dto)
                .collect(Collectors.toList());
    }

    public List<SearchDocumentDto> get() {
        return searchRepository.findAll()
                .stream().map(SearchDocument::dto)
                .collect(Collectors.toList());
    }


}
