package com.hackathon.search.domain;

import com.hackathon.search.dto.SearchDocumentDto;

import static java.util.Objects.requireNonNull;

class SearchDocumentCreator {

    SearchDocument from(SearchDocumentDto searchDocumentDto) {
        requireNonNull(searchDocumentDto);
        return SearchDocument.builder()
                .id(searchDocumentDto.getId())
                .objectId(searchDocumentDto.getObjectId())
                .value(searchDocumentDto.getValue())
                .type(searchDocumentDto.getType())
                .build();
    }

    SearchDocument from(SearchDocumentDto searchDocumentDto, long id) {
        requireNonNull(searchDocumentDto);
        return SearchDocument.builder()
                .id(id)
                .objectId(searchDocumentDto.getObjectId())
                .value(searchDocumentDto.getValue())
                .type(searchDocumentDto.getType())
                .build();
    }

}
