package com.hackathon.search.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class SearchConfiguration {

    @Bean
    SearchFacade searchFacade(SearchRepository searchRepository){
        SearchDocumentCreator searchDocumentCreator = new SearchDocumentCreator();
        return new SearchFacade(searchRepository, searchDocumentCreator);
    }

}
