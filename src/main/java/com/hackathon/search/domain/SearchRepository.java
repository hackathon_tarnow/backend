package com.hackathon.search.domain;

import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SearchRepository extends MongoRepository<SearchDocument, Long> {

    List<SearchDocument> findAllBy(TextCriteria criteria);

    void deleteByObjectId(Long id);
}
