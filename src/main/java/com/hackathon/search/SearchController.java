package com.hackathon.search;

import com.hackathon.search.domain.SearchFacade;
import com.hackathon.search.dto.SearchDocumentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/search")
public class SearchController {

    @Autowired
    private SearchFacade searchFacade;

    @GetMapping
    public ResponseEntity<List<SearchDocumentDto>> find(@RequestParam String search) {
        return ResponseEntity.ok(searchFacade.find(search));
    }

    @GetMapping("/all")
    public ResponseEntity<List<SearchDocumentDto>> get() {
        return ResponseEntity.ok(searchFacade.get());
    }

}
