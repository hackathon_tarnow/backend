package com.hackathon.search.dto;

public enum DocumentType {
    PLACE, NEWS, EVENT, REPORT
}
