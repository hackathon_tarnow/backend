package com.hackathon.search.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Builder
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SearchDocumentDto {

    Long id;
    String value;
    Long objectId;
    DocumentType type;

}