package com.hackathon.config;

import com.hackathon.event.dto.EventNotFoundException;
import com.hackathon.news.dto.NewsNotFoundException;
import com.hackathon.place.dto.PlaceNotFoundException;
import com.hackathon.report.dto.ReportNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Slf4j
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EventNotFoundException.class)
    protected ResponseEntity<Void> handleEventNotFoundException(EventNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(PlaceNotFoundException.class)
    protected ResponseEntity<Void> handlePlaceNotFoundException(PlaceNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(NewsNotFoundException.class)
    protected ResponseEntity<Void> handleNewsNotFoundException(NewsNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(ReportNotFoundException.class)
    protected ResponseEntity<Void> handleNewsNotFoundException(ReportNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

}
