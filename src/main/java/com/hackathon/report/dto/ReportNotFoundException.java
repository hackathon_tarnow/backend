package com.hackathon.report.dto;

public class ReportNotFoundException extends RuntimeException {
    public ReportNotFoundException(Long id) {
        super("No report with id" + id + " found", null, false, false);
    }
}