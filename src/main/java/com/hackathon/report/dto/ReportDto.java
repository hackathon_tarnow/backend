package com.hackathon.report.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Builder
@Getter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReportDto {

    long id;
    String title;
    String application;
    String file;
    Double latitude;
    Double longitude;


}
