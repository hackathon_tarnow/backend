package com.hackathon.report.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ReportConfiguration {

    @Bean
    ReportFacade reportFacade(ReportRepository reportRepository){
        ReportCreator reportCreator = new ReportCreator();
        return new ReportFacade(reportRepository, reportCreator);
    }

}
