package com.hackathon.report.domain;

import com.hackathon.report.dto.ReportDto;

class ReportCreator {

    Report from(ReportDto reportDto) {
        return Report.builder().id(reportDto.getId())
                .application(reportDto.getApplication())
                .latitude(reportDto.getLatitude())
                .longitude(reportDto.getLongitude())
                .file(reportDto.getFile())
                .title(reportDto.getTitle()).build();
    }

    Report from(ReportDto reportDto, long id) {
        return Report.builder().id(id)
                .application(reportDto.getApplication())
                .latitude(reportDto.getLatitude())
                .longitude(reportDto.getLongitude())
                .file(reportDto.getFile())
                .title(reportDto.getTitle()).build();
    }

}
