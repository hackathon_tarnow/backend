package com.hackathon.report.domain;

import com.hackathon.config.DeletedEvent;
import com.hackathon.config.SequenceGeneratorService;
import com.hackathon.report.dto.ReportDto;
import com.hackathon.report.dto.ReportNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.mongodb.core.query.TextCriteria;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

public class ReportFacade {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    private ReportRepository reportRepository;
    private ReportCreator reportCreator;

    @Autowired
    SequenceGeneratorService sequenceGeneratorService;

    public ReportFacade(ReportRepository reportRepository, ReportCreator reportCreator) {
        this.reportRepository = reportRepository;
        this.reportCreator = reportCreator;
    }

    public List<ReportDto> get() {
        return reportRepository.findAll()
                .stream().map(Report::dto)
                .collect(Collectors.toList());
    }

    public ReportDto get(Long id) {
        return reportRepository.findById(id)
                .orElseThrow(() -> new ReportNotFoundException(id))
                .dto();
    }

    public void add(ReportDto reportDto) {
        requireNonNull(reportDto);
        long id = sequenceGeneratorService.generateSequence(Report.SEQUENCE_NAME);
        Report report = reportRepository.save(reportCreator.from(reportDto, id));
        applicationEventPublisher.publishEvent(report.dto());
    }

    public void delete(long id) {
        reportRepository.deleteById(id);
        applicationEventPublisher.publishEvent(new DeletedEvent(id));
    }

    public List<ReportDto> find(String searchParam) {
        TextCriteria criteria = TextCriteria.forDefaultLanguage()
                .matchingAny(searchParam);
        return reportRepository.findAllBy(criteria)
                .stream().map(Report::dto)
                .collect(Collectors.toList());
    }

}
