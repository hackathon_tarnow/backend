package com.hackathon.report.domain;

import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

interface ReportRepository extends MongoRepository<Report, Long> {

    List<Report> findAllBy(TextCriteria criteria);
}
