package com.hackathon.report.domain;

import com.hackathon.report.dto.ReportDto;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Builder
@Document(collection = "report")
@FieldDefaults(level = AccessLevel.PRIVATE)
class Report {

    @Id
    long id;
    @NotNull
    @TextIndexed
    String title;
    @NotNull
    String application;
    String file;
    Double latitude;
    Double longitude;

    @Transient
    public static final String SEQUENCE_NAME = "report_sequence";

    ReportDto dto() {
        return ReportDto.builder().id(id)
                .title(title)
                .file(file)
                .latitude(latitude)
                .longitude(longitude)
                .application(application)
                .build();
    }


}
