package com.hackathon.report;

import com.hackathon.place.domain.Place;
import com.hackathon.place.dto.PlaceDto;
import com.hackathon.report.domain.ReportFacade;
import com.hackathon.report.dto.ReportDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/reports")
public class ReportController {

    @Autowired
    private ReportFacade reportFacade;

    @GetMapping
    public ResponseEntity<List<ReportDto>> get(@RequestParam(required = false) String search) {
        if (search != null) {
            return ResponseEntity.ok(reportFacade.find(search));
        }
        return ResponseEntity.ok(reportFacade.get());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ReportDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(reportFacade.get(id));
    }

    @PostMapping
    public ResponseEntity<Void> add(@Valid @RequestBody ReportDto reportDto) {
        reportFacade.add(reportDto);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/batch")
    public ResponseEntity<Void> add(@RequestBody List<ReportDto> reportDto) {
        reportDto.forEach(reportFacade::add);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        reportFacade.delete(id);
        return ResponseEntity.noContent().build();
    }

}
