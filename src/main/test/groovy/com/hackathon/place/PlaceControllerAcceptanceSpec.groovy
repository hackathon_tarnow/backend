package com.hackathon.place

import com.hackathon.base.IntegrationSpec
import com.hackathon.place.domain.PlaceFacade
import com.hackathon.place.domain.SamplePlace
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.ResultActions

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class PlaceControllerAcceptanceSpec extends IntegrationSpec implements SamplePlace {

    @Autowired
    PlaceFacade placeFacade;

    @WithMockUser
    def "should get places"() {
        given: 'system has localCluster and productionCluster'
            placeFacade.add(theater)
            placeFacade.add(cinema)
        when: 'I go to /places'
            ResultActions getPlaces = mockMvc.perform(get("/places"))
        then: 'I see places list'
            mockMvc.perform(get("/places")).andExpect(status().isOk())
                .andExpect(content().json("""
                    [
                            $theaterJson,
                            $cinemaJson
                    ]"""))

    }

}
