package com.hackathon.place.domain

import com.hackathon.place.dto.PlaceDto
import groovy.json.JsonBuilder

trait SamplePlace {

    PlaceDto theater = PlaceDto.builder()
            .id(1)
            .name("teatr")
            .description("super teatr")
            .latitude(32.32)
            .build();

    PlaceDto cinema = PlaceDto.builder()
            .id(2)
            .name("kino")
            .description("super kino")
            .latitude(32.32)
            .build();


    def theaterJson = new JsonBuilder(theater).toString()
    def cinemaJson = new JsonBuilder(cinema).toString()
}
