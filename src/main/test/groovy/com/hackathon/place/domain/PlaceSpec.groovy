package com.hackathon.place.domain

import groovy.json.JsonBuilder
import spock.lang.Specification

class PlaceSpec extends Specification implements SamplePlace {

    PlaceFacade facade = new PlaceConfiguration().placeFacade();

    def "should add theater"() {
        when: "we add a theater"
            facade.add(theater);
        then: "system has theater"
            facade.get(1L) == theater;
    }

    def "should return list of places"() {
        when: "we add a theater and cinema"
            facade.add(cinema);
            facade.add(theater);
        then: "system return theater and cinema"
            facade.get().sort() == [cinema, theater].sort();
    }

}
