package com.hackathon.event

import com.hackathon.base.IntegrationSpec
import com.hackathon.event.domain.EventFacade
import com.hackathon.event.domain.SampleEvent
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.ResultActions

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class EventControllerAcceptanceSpec extends IntegrationSpec implements SampleEvent{

    @Autowired
    EventFacade eventFacade

    def "positive event scenario"() {
        given: 'system has an old event "Stare wydarzenie" and a new event "Nowe Wydarzenie"'
        eventFacade.addNewEvent(oldEvent)
        eventFacade.addNewEvent(newEvent)

        when: 'I go to /events'
        ResultActions getEvents = mockMvc.perform(get("/events"))
        then: 'I see both events'
        getEvents.andExpect(status().isOk())
                .andExpect(content().json("""
                [
                    $oldEventJson,
                    $newEventJson
                ]"""))

    }

}