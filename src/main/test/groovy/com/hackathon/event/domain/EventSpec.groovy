package com.hackathon.event.domain


import com.hackathon.event.dto.EventDto
import spock.lang.Specification

class EventSpec extends Specification implements SampleEvent{
    private EventFacade eventFacade = new EventConfigurator().eventFacade()

    def "should add new event"(){
        given: "i want add new event 'Nowe Spotkanie'"
        EventDto eventDto = newEvent
        when: "i add new event"
        EventDto event = eventFacade.addNewEvent(eventDto)
        then: "Event 'Nowe Spotkanie' should be added"
        eventFacade.get().size() == 1
        event.name == eventDto.name
    }

    def "should list all events"() {
        given: "in system are 2 events"
        eventFacade.addNewEvent(oldEvent)
        eventFacade.addNewEvent(newEvent)
        when: "i ask for events"
        List<EventDto> events = eventFacade.get()
        then: "system should return 2 event"
        events.sort() == [newEvent, oldEvent].sort()
    }


}
