package com.hackathon.event.domain

import com.hackathon.event.dto.EventDto
import groovy.json.JsonBuilder


trait SampleEvent {
    EventDto oldEvent = EventDto.builder()
            .id(1L)
            .name('Stare Wydarzenie')
            //.eventDate(new DateTime(2019,1,1,1,1))
            .fileName("oldFileName")
            .eventDescription("stareWydarzenieOpis")
            .longitude(12.12)
            .latitude(73.75)
            .build()

    EventDto newEvent = EventDto.builder()
            .id(2L)
            .name('Nowe Wydarzenie')
            //.eventDate(new DateTime(2019,1,1,1,1))
            .fileName("newFileName")
            .eventDescription("noweWydarzenieOpis")
            .longitude(32.32)
            .latitude(23.45)
            .build()

    def oldEventJson = new JsonBuilder(oldEvent).toString()
    def newEventJson = new JsonBuilder(newEvent).toString()
}
